class istartup::profile::base (
  $ntp_servers = undef,
) inherits ::istartup::params {

  require ::istartup::profile

  if undef != $ntp_servers { validate_array($ntp_servers) }
  class { 'ntp': servers => $ntp_servers, }

  if 'RedHat' == $::osfamily { require ::epel }
}
