# == Class istartup::params
#
# This class is meant to be called from istartup.
# It sets variables according to platform.
#
class istartup::params {
  case $::osfamily {
    'Debian','RedHat': {
    }
    default: {
      fail("${::operatingsystem} not supported")
    }
  }
}
