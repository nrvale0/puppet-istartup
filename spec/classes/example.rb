require 'spec_helper'

describe 'istartup' do
  context 'supported operating systems' do
    on_supported_os.each do |os, facts|
      context "on #{os}" do
        let(:facts) do
          facts
        end

        context "istartup class without any parameters" do
          let(:params) {{ }}

          it { is_expected.to compile.with_all_deps }

          it { is_expected.to contain_class('istartup::params') }
          it { is_expected.to contain_class('istartup::install').that_comes_before('istartup::config') }
          it { is_expected.to contain_class('istartup::config') }
          it { is_expected.to contain_class('istartup::service').that_subscribes_to('istartup::config') }

          it { is_expected.to contain_service('istartup') }
          it { is_expected.to contain_package('istartup').with_ensure('present') }
        end
      end
    end
  end

  context 'unsupported operating system' do
    describe 'istartup class without any parameters on Solaris/Nexenta' do
      let(:facts) {{
        :osfamily        => 'Solaris',
        :operatingsystem => 'Nexenta',
      }}

      it { expect { is_expected.to contain_package('istartup') }.to raise_error(Puppet::Error, /Nexenta not supported/) }
    end
  end
end
