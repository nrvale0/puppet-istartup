require 'spec_helper'

describe 'istartup::profile::base' do
  context 'supported operating systems' do

    default_facts = { 'is_virtual' => 'false', }

    os_facts = { 
      'RedHat6' => { 'osfamily' => 'RedHat', 'operatingsystem' => 'RedHat', 'operatingsystemmajrelease' => '6' },
      'RedHat7' => { 'osfamily' => 'RedHat', 'operatingsystem' => 'RedHat', 'operatingsystemmajrelease' => '7' },
      'CentOS6' => { 'osfamily' => 'RedHat', 'operatingsystem' => 'CentOS', 'operatingsystemmajrelease' => '6' },
      'CentOS7' => { 'osfamily' => 'RedHat', 'operatingsystem' => 'CentOS', 'operatingsystemmajrelease' => '7' },
      'Ubuntu1404' => { 'osfamily' => 'Debian', 'operatingsystem' => 'Ubuntu', 'operatingsystemmajrelease' => '14', }
    }

    os_facts.each do |os, facts|
      context "on #{os}" do
        let(:facts) do
          facts.merge!(default_facts)
        end

        context "istartup::profile::base with defaults" do
          ntp_service_name = 'ntp'
          if 'Debian' == facts[:osfamily] then ntp_service_name = 'ntpd' end

          describe "without any parameters" do
            let(:params) {{ }}

            # basic sanity checks
            it { is_expected.to compile.with_all_deps }

            it { is_expected.to contain_class('istartup::params') }
            it { is_expected.to contain_class('istartup') }
            it { is_expected.to contain_class('istartup::profile') }
            it { is_expected.to contain_class('istartup::profile::base') }

            # functional requirements for the base profile
            it { is_expected.to contain_service(ntp_service_name).with(
              'ensure' => 'running',
              'enable' => 'true'
            ) }
          end
        end

      end
    end
  end

  context 'unsupported operating system' do
    describe 'istartup class without any parameters on Solaris/Nexenta' do
      let(:facts) {{ :osfamily => 'Solaris', :operatingsystem => 'Nexenta', }}

      it { expect { catalogue }.to raise_error(Puppet::Error, /not supported/) }
    end
  end
end
