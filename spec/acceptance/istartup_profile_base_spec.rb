require 'spec_helper_acceptance'

describe 'istartup::profile::base' do
  context 'supported operating systems' do

    default_facts = { 'is_virtual' => 'false', }

    os_facts = { 
      'RedHat6' => { 'osfamily' => 'RedHat', 'operatingsystem' => 'RedHat', 'operatingsystemmajrelease' => '6' },
      'RedHat7' => { 'osfamily' => 'RedHat', 'operatingsystem' => 'RedHat', 'operatingsystemmajrelease' => '7' },
      'CentOS6' => { 'osfamily' => 'RedHat', 'operatingsystem' => 'CentOS', 'operatingsystemmajrelease' => '6' },
      'CentOS7' => { 'osfamily' => 'RedHat', 'operatingsystem' => 'CentOS', 'operatingsystemmajrelease' => '7' },
      'Ubuntu1404' => { 'osfamily' => 'Debian', 'operatingsystem' => 'Ubuntu', 'operatingsystemmajrelease' => '14', }
    }

    os_facts.each do |os, facts|
      context "on #{os}" do
        let(:facts) do
          facts.merge!(default_facts)
        end

        ntp_service_name = 'ntp'
        if 'Debian' == facts[:osfamily] then ntp_service_name = 'ntpd' end

        context "istartup::profile::base with defaults" do
            it 'should work idempotently with no errors' do
            pp = <<-EOS
              class { '::istartup::profile::base': }
            EOS

            apply_manifest(pp, :catch_failures => true)
            apply_manifest(pp, :catch_changes => true)
          end
        end

        describe service(ntp_service_name) do
          it { is_expected.to be_running }
        end

        describe port('123') do
          it { should be_listening.with('udp') }
        end
      end
    end
  end
end

